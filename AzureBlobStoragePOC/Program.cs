﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace blob_storage_poc
{
    class Program
    {
        static string input;
        static string filePathForUpload, filePathForUploadDirectory, filePathForUploadFilename;
        static List<string> validMenuItems = new List<string> { "1", "2", "3", "4", "5", "6" };
        static bool wrongContainerName;

        public static async Task Main(string[] args)
        {
            PrintIntro();
            await ProcessUserMenuItemDecision();
        }

        static string GetConnectionString()
        {
            return "DefaultEndpointsProtocol=https;AccountName=azureblobstoragepoc;AccountKey=wni9qqPgLGFMcN/qWcgs6c7DhSYpZY3EXPKJPxIXSEE6ZvW7Ou10FkznnmIlv936hziVBLbxpQXbresIvYA2fA==;EndpointSuffix=core.windows.net";
        }

        static void PrintIntro()
        {
            Console.WriteLine("\n \tAZURE BLOB STORAGE DEMO");
            Console.WriteLine("\t----------------------- \n");
            Console.WriteLine("\t1. Maak een container");
            Console.WriteLine("\t2. Toon alle containers");
            Console.WriteLine("\t3. Upload een bestand");
            Console.WriteLine("\t4. Download een bestand");
            Console.WriteLine("\t5. Toon bestanden van één specifieke container");
            Console.WriteLine("\t6. Toon bestanden op basis van een trefwoord van één specifieke container \n");
        }

        static async Task CreateContainer(BlobServiceClient client)
        {
            Console.WriteLine("\n\t----------------------- \n");
            Console.WriteLine("\tGeef een naam op voor de container. \n");
            input = Console.ReadLine();
            Dictionary<string, string> metadata = new Dictionary<string, string>();
            metadata.Add("TestKey", "TestValue");
            try
            {
                await client.CreateBlobContainerAsync(input, PublicAccessType.None, metadata, CancellationToken.None);
                Console.WriteLine("\n\tContainer aan het maken...");
                Console.WriteLine("\n\tContainer aangemaakt.");
                Console.WriteLine("\n\t----------------------- \n");
                await ProcessUserMenuItemDecision();

            }
            catch (RequestFailedException rfex)
            {
                if (rfex.Status == 409)
                {
                    Console.WriteLine($"\n\tEr bestaat al een container met de naam {input}, geef een andere naam op.");
                    await CreateContainer(client);
                }
            }
        }

        static async Task GetContainers(BlobServiceClient client)
        {
            try
            {
                Console.WriteLine("\n \tContainers aan het ophalen...");
                Console.WriteLine("\n\t----------------------- \n");
                await PrintContainerInfo(client);
            }
            catch (RequestFailedException rfex)
            {
                Console.WriteLine($"\n\tEr ging iets mis bij het ophalen van de containers:\n\tMessage:{rfex.Message}\n\tStatusCode:{rfex.Status}");
            }

            await ProcessUserMenuItemDecision();
        }

        static async Task UploadBlob(BlobServiceClient client)
        {
            Console.WriteLine("\n\t----------------------- \n");
            try
            {
                Console.WriteLine("\n \tGeef het pad op waar het up te loaden bestand staat.\n");
                filePathForUpload = Console.ReadLine();

                if (!File.Exists(filePathForUpload.Replace("\"", "")))
                {
                    Console.WriteLine($"\n \tHet bestand '{filePathForUpload}' kan niet worden gevonden. Geef een geldige pad op.");
                    await UploadBlob(client);
                }
                else
                {
                    Console.WriteLine("\n\tBestand gevonden!");
                    Console.WriteLine("\n\tGeef de naam van de container op waarin u het bestand wil plaatsen.\n");
                    await PrintContainerInfo(client);

                    AsyncPageable<BlobContainerItem> containers = client.GetBlobContainersAsync();

                    do
                    {
                        input = Console.ReadLine();

                        await foreach (BlobContainerItem container in containers)
                        {
                            if (container.Name.Equals(input))
                            {
                                Console.WriteLine($"\n\tContainer gevonden!");
                                wrongContainerName = false;
                                BlobContainerClient containerClient = new BlobContainerClient(GetConnectionString(), container.Name);

                                int indexOfLastSlashInFolderPath = filePathForUpload.LastIndexOf('\\');
                                filePathForUploadDirectory = filePathForUpload.Substring(1, indexOfLastSlashInFolderPath);
                                filePathForUploadFilename = filePathForUpload.Substring(indexOfLastSlashInFolderPath + 1, (filePathForUpload.Length - indexOfLastSlashInFolderPath) - 2);
                                Directory.SetCurrentDirectory(filePathForUploadDirectory);
                                Stream stream = File.OpenRead(filePathForUploadFilename);

                                Console.WriteLine($"\n\tBestand uploaden...");
                                string blobFileName = $"C/100119/images/{filePathForUploadFilename}";
                                await containerClient.UploadBlobAsync(blobFileName, stream);
                                Console.WriteLine($"\n\tUploaden van BLOB {blobFileName} naar container '{container.Name}' is voltooid.");
                                Console.WriteLine("\n\t----------------------- \n");

                                break;
                            }
                            else
                                wrongContainerName = true;

                        }

                        if (wrongContainerName)
                            Console.WriteLine($"\n\tContainer '{input}' does not exist. Please provide a valid containername.\n");

                    } while (wrongContainerName);
                }
            }
            catch (RequestFailedException rfex)
            {
                Console.WriteLine($"\n\tEr bestaat al een BLOB met de naam C/100119/images/{filePathForUploadFilename}. Probeer opnieuw\n");
                await UploadBlob(client);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"\n\tEr ging iets mis bij het uploaden van het bestand.\n\tMessage:{ex.Message}");
            }

            await ProcessUserMenuItemDecision();
        }

        static async Task GetBlobsFromASpecificContainer(BlobServiceClient client)
        {
            Console.WriteLine("\n\t----------------------- \n");
            Console.WriteLine("\n\tGeef de naam van de container op waarvan u bestanden wilt ophalen.\n");

            try
            {
                await PrintContainerInfo(client);

                AsyncPageable<BlobContainerItem> containers = client.GetBlobContainersAsync();

                do
                {
                    input = Console.ReadLine();

                    await foreach (BlobContainerItem container in containers)
                    {
                        if (container.Name.Equals(input))
                        {
                            Console.WriteLine($"\n\tContainer gevonden!");
                            wrongContainerName = false;
                            BlobContainerClient containerClient = new BlobContainerClient(GetConnectionString(), container.Name);

                            Console.WriteLine($"\n\tBlobs aan het ophalen...");

                            BlobClient blobClient = null;
                            AsyncPageable<BlobItem> blobs = containerClient.GetBlobsAsync();
                            await foreach (BlobItem blob in blobs)
                            {
                                blobClient = containerClient.GetBlobClient(blob.Name);
                                Console.WriteLine($"\n\tBestandsnaam: {blob.Name}\n\tURL: {blobClient.Uri.ToString()}\n\tLast Modified: {blob.Properties.LastModified}\n");
                            }

                            break;
                        }
                        else
                        {
                            wrongContainerName = true;
                        }

                    }

                    if (wrongContainerName)
                        Console.WriteLine($"\n\tContainer '{input}' does not exist. Please provide a valid containername.\n");

                } while (wrongContainerName);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"\n\tEr ging iets mis bij het ophalen van bestanden.\n\tMessage:{ex.Message}");
            }

            await ProcessUserMenuItemDecision();
        }

        static async Task GetBlobsFromASpecificContainerByKeyword(BlobServiceClient client)
        {
            Console.WriteLine("\n\t----------------------- \n");
            Console.WriteLine("\n\tGeef de naam van de container op waarvan u de bestanden wil ophalen.\n");

            try
            {
                await PrintContainerInfo(client);

                AsyncPageable<BlobContainerItem> containers = client.GetBlobContainersAsync();

                do
                {
                    input = Console.ReadLine();

                    await foreach (BlobContainerItem container in containers)
                    {
                        if (container.Name.Equals(input))
                        {
                            Console.WriteLine($"\n\tContainer gevonden!");
                            wrongContainerName = false;
                            BlobContainerClient containerClient = new BlobContainerClient(GetConnectionString(), container.Name);

                            //Specifies a string that filters the results to return only blobs whose name begins with the specified prefix.
                            Console.WriteLine($"\n\tGeef een zoekopdracht op die het resultaat filtert om enkel bestanden terug te geven welke hun bestandsnaam begint met de opgegeven prefix.");
                            input = Console.ReadLine();
                            Console.WriteLine($"\n\tBestanden aan het ophalen...");

                            AsyncPageable<BlobItem> blobs = containerClient.GetBlobsAsync(prefix: input);
                            bool hasItems = false;

                            await foreach (BlobItem blob in blobs)
                            {
                                hasItems = true;
                                Console.WriteLine($"\n\t{blob.Name} ({blob.Properties.LastModified})");
                            }

                            if (!hasItems)
                            {
                                Console.WriteLine($"\n\tEr zijn geen bestanden waarvan de bestandsnaam begint met {input}.");
                            }

                            break;
                        }
                        else
                        {
                            wrongContainerName = true;
                        }

                    }

                    if (wrongContainerName)
                        Console.WriteLine($"\n\tContainer '{input}' bestaat niet. Gelieve een geldige containernaam op te geven.\n");

                } while (wrongContainerName);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"\n\tEr ging iets mis bij het ophalen van bestanden.\n\tMessage:{ex.Message}");
            }

            await ProcessUserMenuItemDecision();
        }

        static async Task DownloadBlob(BlobServiceClient client)
        {
            Console.WriteLine("\n\t----------------------- \n");
            Console.WriteLine("\n\tGeef de naam van de container op waarvan u een bestand wil downloadn.\n");

            try
            {
                await PrintContainerInfo(client);

                AsyncPageable<BlobContainerItem> containers = client.GetBlobContainersAsync();

                do
                {
                    input = Console.ReadLine();

                    await foreach (BlobContainerItem container in containers)
                    {
                        if (container.Name.Equals(input))
                        {
                            Console.WriteLine($"\n\tContainer gevonden!");
                            wrongContainerName = false;
                            BlobContainerClient containerClient = new BlobContainerClient(GetConnectionString(), container.Name);

                            //Specifies a string that filters the results to return only blobs whose name begins with the specified prefix.
                            Console.WriteLine($"\n\tVolgende bestanden zijn aanwezig in container '{container.Name}'\n");

                            BlobClient blobClient = null;
                            AsyncPageable<BlobItem> blobs = containerClient.GetBlobsAsync();
                            bool hasItems = false;
                            await foreach (BlobItem blob in blobs)
                            {
                                hasItems = true;
                                blobClient = containerClient.GetBlobClient(blob.Name);
                                Console.WriteLine($"\n\tBestandsnaam: {blob.Name}\n\tURL: {blobClient.Uri.ToString()}\n\tLast Modified: {blob.Properties.LastModified}\n");
                            }

                            if (!hasItems)
                            {
                                Console.WriteLine($"\n\tContainer '{container.Name}' bevat geen bestanden.");
                            }
                            else
                            {
                                Console.WriteLine($"\n\tGeef de bestandsnaam op van het bestand dat u wilt downloaden.\n");
                                input = Console.ReadLine();
                                bool foundBlob = false;
                                await foreach (BlobItem blob in blobs)
                                {
                                    if (blob.Name.Equals(input))
                                    {
                                        Console.WriteLine("\n\tBestand gevonden!");
                                        foundBlob = true;
                                        int indexOfLastSlashInFilename = blob.Name.LastIndexOf("/");
                                        string fileNameForDownloadedFile = blob.Name.Substring(indexOfLastSlashInFilename + 1);
                                        string filePathForDownloadedFile = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + $"\\{fileNameForDownloadedFile}";
                                        await blobClient.DownloadToAsync(filePathForDownloadedFile);

                                        Console.WriteLine($"\n\tBestanden aan het downloaden...");
                                        Console.WriteLine($"\n\tBestand gedownloadt naar {filePathForDownloadedFile}");
                                        break;
                                    }
                                }

                                if (!foundBlob)
                                    Console.WriteLine($"\n\tBestand met bestandsnaam '{input}' bestaat niet.");

                            }

                            break;
                        }
                        else
                        {
                            wrongContainerName = true;
                        }

                    }

                    if (wrongContainerName)
                        Console.WriteLine($"\n\tContainer '{input}' bestaat niet. Gelieve een geldige containernaam op te geven.\n");

                } while (wrongContainerName);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"\n\tEr ging iets mis bij het downloaden van het bestand.\n\tMessage:{ex.Message}");
            }

            await ProcessUserMenuItemDecision();
        }

        static async Task PrintContainerInfo(BlobServiceClient client)
        {
            AsyncPageable<BlobContainerItem> containers = client.GetBlobContainersAsync();
            await foreach (BlobContainerItem item in containers)
            {
                Console.WriteLine($"\t{item.Name} (last modified: {item.Properties.LastModified})");
            }
            Console.WriteLine();
        }

        static async Task ProcessUserMenuItemDecision()
        {
            wrongContainerName = false;
            BlobServiceClient client = new BlobServiceClient(GetConnectionString());

            Console.WriteLine("\n\tMaak een keuze:\n");
            input = Console.ReadLine();
            if (!validMenuItems.Contains(input))
            {
                Console.WriteLine($"\t'{input}' is geen geldig menu item.\n");
                await ProcessUserMenuItemDecision();
            }

            if (input.Equals("1"))
            {
                await CreateContainer(client);
            }
            else if (input.Equals("2"))
            {
                await GetContainers(client);
            }
            else if (input.Equals("3"))
            {
                await UploadBlob(client);
            }
            else if (input.Equals("4"))
            {
                await DownloadBlob(client);
            }
            else if (input.Equals("5"))
            {
                await GetBlobsFromASpecificContainer(client);
            }
            else if (input.Equals("6"))
            {
                await GetBlobsFromASpecificContainerByKeyword(client);
            }
        }
    }
}
